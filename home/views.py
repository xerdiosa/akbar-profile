from django.shortcuts import render


def home(request):
    return render(request, "home.html")

def about_me(request):
    return render(request, "about-me.html")

def experiences(request):
    return render(request, "experiences.html")

def achievements(request):
    return render(request, "achievements.html")

def contact_me(request):
    return render(request, "contact-me.html")

def bonus(request):
    return render(request, "bonus.html")