from django.urls import path

from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('about-me/', views.about_me, name='about_me'),
    path('experiences/', views.experiences, name='experiences'),    
    path('achievements/', views.achievements, name='achievements'),
    path('contact-me/', views.contact_me, name='contact-me'),
    path('apod/', views.bonus, name='apod'),
]